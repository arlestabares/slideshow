import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:slideshow/src/theme/theme.dart';
import 'package:slideshow/src/page/launcher_page.dart';
// import 'package:slideshow/src/page/emergency_page.dart';
// import 'package:slideshow/src/page/pinteres_page.dart';
// import 'package:slideshow/src/page/slideshow_page.dart';
// import 'package:slideshow/src/page/slider_list_page.dart';
//import 'package:slideshow/page/pinteres_page.dart';

//import 'package:slideshow/page/slideshow_page.dart';
//import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
void main() => runApp(
//Pongo este NotifierChangeProvider en la parte mas alta del arbol de widgets
//para que toda mi aplicacion  escuche los cambios
    ChangeNotifierProvider(create: (_) => new ThemeChanger(2), child: MyApp()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
  final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return MaterialApp(
    
    theme: currentTheme,
      debugShowCheckedModeBanner: false,
      title: 'SlideShow',
      // home: EmergencyPage()
      initialRoute: 'launchPage',
      routes: <String, WidgetBuilder>{
        // '/': (BuildContext context) => SlideshowPage(),
        // 'pinteres': (BuildContext context) => PinteresPage(),
        // 'emergency': (BuildContext context) => EmergencyPage(),
        // 'sliverPage': (BuildContext context) => SliverListPage(),
        'launchPage': (BuildContext context) => LaunchPage(),
      },
    );
  }
}
