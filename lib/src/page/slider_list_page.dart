import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:slideshow/src/theme/theme.dart';

class SliverListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //body: _TitleHeaderMain(),
        //body: _ListMenuItems(),
        body: Stack(
      children: <Widget>[
        //_ListMenuItems(),
        // _TitleHeaderMain(),
        _MainScroll(),
        Positioned(bottom: -10, right: 0, child: _BottonNewList()),
      ],
    ));
  }
}

class _BottonNewList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
    final size = MediaQuery.of(context).size;
    final appTheme = Provider.of<ThemeChanger>(context);

    return ButtonTheme(
      minWidth: size.width * 0.9,
      height: 100,
      child: RaisedButton(
        onPressed: () {},
        color: (appTheme.darkTheme)
            ? appTheme.currentTheme.accentColor
            : Color(0xffED6762),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(50))),
        child: Text(
          'CREATE NEW LIST',
          style: TextStyle(
              color: appTheme.currentTheme.scaffoldBackgroundColor,
              fontSize: 18,
              fontWeight: FontWeight.bold,
              letterSpacing: 3),
        ),
      ),
    );
  }
}

//Widget Contenedor del titulo,color, y de la configuracion
//del CustomScrollView que contiene un arreglo de sliver de la
//clase _SliverCustomHeaderDelegate(),
//que permite que el appBar aparezca o desaparezca
class _MainScroll extends StatelessWidget {
  final items = [
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
  ];

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return CustomScrollView(
      physics: BouncingScrollPhysics(),
      slivers: <Widget>[
        // SliverAppBar(
        // floating: true,
        //   backgroundColor: Colors.red,
        //   title: Text('Lista de items'),
        // ),
        SliverPersistentHeader(
            floating: true,
            delegate: _SliverCustomHeaderDelegate(
                minheight: 200,
                maxheight: 350,
                child: Container(
                  //alineacion del titulo del encabezado
                  alignment: Alignment.centerLeft,
                  // color: Colors.blue,
                  color: appTheme.scaffoldBackgroundColor,
                  child: _TitleHeaderMain(),
                ))),

        SliverList(
            delegate: SliverChildListDelegate([
          //debemos anexar [], ya que de lo contrario arrojara error
          //Usamos el operador expread para extraer cada uno de los items de lo contrario
          //tendremos un error, y
          //ponemos un SizeBox para emular un item mas invisible, asi tendremos el espacio
          //suficiente para poner el boton flotante al final de la misma lista
          ...items,
          SizedBox(height: 100)
        ]))
      ],
    );
  }
}

//clase que me permite al subir la lista de items, que desaparezca la appBar(),
//asi mismo que aparezca al bajar la lista
class _SliverCustomHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double minheight;
  final double maxheight;
  final Widget child;

//Los argumentos no son posicionales, son por nombre y obligatorios
  _SliverCustomHeaderDelegate(
      {@required this.minheight,
      @required this.maxheight,
      @required this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  //double get maxExtent => (minheight > maxExtent )? minheight : minheight;
  double get maxExtent => minheight;

  @override
  double get minExtent => minheight;

  @override
  bool shouldRebuild(covariant _SliverCustomHeaderDelegate oldDelegate) {
    return maxheight != oldDelegate.maxheight ||
        minheight != oldDelegate.minheight ||
        child != oldDelegate.child;
  }
}

//Widget encargado de la configuracion del titulo y encabezado
class _TitleHeaderMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
    final appTheme = Provider.of<ThemeChanger>(context);
    
    return Column(
      children: <Widget>[
        // SizedBox(height: 30),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 30, vertical: 40),
          child: Text(
            'FerreAgro',
            style: TextStyle(
                color: (appTheme.darkTheme) ? Colors.grey : Color(0xff532128),
                fontSize: 50),
          ),
        ),
        Stack(
          children: <Widget>[
            SizedBox(width: 100),
            Positioned(
              bottom: 8,
              child: Container(
                width: 400,
                height: 3,
                color: (appTheme.darkTheme) ? Colors.grey : Color(0xffF7CDD5),
              ),
            ),
            Container(
              //margin: EdgeInsets.zero,
              child: Text('Su Ferreteria de Confianza ',
                  style: TextStyle(
                      color: ( appTheme.darkTheme ) ? appTheme.currentTheme.accentColor :  Color(0xffD93A30),
                      fontSize: 30,
                      fontWeight: FontWeight.bold)),
            ),
          ],
        )
      ],
    );
  }
}

//Configuracion del numero de _ListItems de la lista
class _ListMenuItems extends StatelessWidget {
  final items = [
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
    _ListItem('Orange', Color(0xffF08F66)),
    _ListItem('Family', Color(0xffF2A38A)),
    _ListItem('Subscriptions', Color(0xffF7CDD5)),
    _ListItem('Books', Color(0xffFCEBAF)),
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) => items[index]);
    // return _ListItem();
  }
}

//Widget encargado de la creacion de un item  o botom de la lista,
//con su color, titulo y margin
class _ListItem extends StatelessWidget {
  final String title;
  final Color color;

  const _ListItem(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);

    return Container(
      child: Text(title, style: TextStyle( color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20)),
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.all(30),
      //Altura de los botones  
      height: 100,
      //width: 100,
      //Este margin le da el ancho de separacion entre items de la lista en 10
      margin: EdgeInsets.all(10),
      decoration:
          BoxDecoration(color: ( appTheme.darkTheme ) ? Colors.blueGrey : color , borderRadius: BorderRadius.circular(30)),
    );
  }
}
