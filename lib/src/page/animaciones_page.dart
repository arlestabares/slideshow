import 'package:flutter/material.dart';
import 'dart:math' as Math;

class AnimacionesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CuadradoAnimado(),
      ),
    );
  }
}

class CuadradoAnimado extends StatefulWidget {
  @override
  _CuadradoAnimadoState createState() => _CuadradoAnimadoState();
}

class _CuadradoAnimadoState extends State<CuadradoAnimado>
    with SingleTickerProviderStateMixin {
//siempre va estar escuchando los cambios
  AnimationController controller;
  Animation<double> rotacion;
  Animation<double> opacidad;
  Animation<double> opacidadOut;
  Animation<double> moverDerecha;
  Animation<double> agrandarCuadrado;

  @override
  void initState() {
    //defino  que tipo de controller deseo.
    controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 4000));
    //Especifico mediante Tween, que tipo de animacion quiero.
    //y debo especificarle quien es el que la controla, en este caso
    //animate(controller)

    //rotacion = Tween(begin: 0.0, end: 2.0 * Math.pi).animate(controller);
    rotacion = Tween(begin: 0.0, end: 2.0 * Math.pi)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeOut));

    //opacidad = Tween(begin: 0.1, end: 1.0).animate(controller);
    opacidad = Tween(begin: 0.1, end: 1.0).animate(CurvedAnimation(
        parent: controller, curve: Interval(0.0, 0.25, curve: Curves.easeOut)));

    opacidadOut = Tween(begin: 0.0, end: 1.0).animate(
    CurvedAnimation(parent: controller,curve:Interval(0.75, 1.0 ,curve: Curves.easeOut)));
    //moverDerecha = Tween(begin: 0.0, end: 200.0).animate(controller);
    moverDerecha = Tween(begin: 0.0, end: 200.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeOut));

    agrandarCuadrado = Tween(begin: 0.0, end: 2.0)
        .animate(CurvedAnimation(parent: controller, curve: Curves.easeOut));

    controller.addListener(() {
      //print('Stado: ${controller.status}');

      if (controller.status == AnimationStatus.completed) {
        //controller.reverse();
       controller.reset();
       // controller.reverse();
      }
      // else if( controller.status == AnimationStatus.dismissed ){
      // controller.forward();

      // }
    });

    super.initState();
  }

  @override
  void dispose() {
    //Cuando ya no utilizamos el controller, podemos desacernos de el
    //para limpiarlo mediante controller,dispose(), asi eviitamos fuga de memoria
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //Play / Reproduccion
    //controller.repeat();
    controller.forward();

    return AnimatedBuilder(
      animation: controller,
      //child: child'
      child: _Rectangulo(),
      builder: (BuildContext context, Widget childRectangulo) {
        // print('rotacion '+ rotacion.value.toString());
           print('Opacidad: ${opacidad.value}');
           print('Rotacion: ${rotacion.status}');
        return Transform.translate(
          offset: Offset(moverDerecha.value, 0),
          child: Transform.rotate(
              angle: rotacion.value,
              // child: _Rectangulo(),
              child: Opacity(
                opacity: opacidad.value - opacidadOut.value,
                child: Transform.scale(
                scale: agrandarCuadrado.value,
                child: childRectangulo),
              )),
        );
      },
    );
  }
}

class _Rectangulo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      decoration: BoxDecoration(color: Colors.blue),
    );
  }
}
