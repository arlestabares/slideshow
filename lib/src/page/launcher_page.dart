import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:slideshow/src/routes/routes.dart';
import 'package:slideshow/src/theme/theme.dart';

class LaunchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: appTheme.currentTheme.accentColor,
          title: Text('Diseños en flutter'),
        ),
        drawer: _MenuPrincipal(),
        body: Stack(
          children: [
            _ListaOpciones(),
            Positioned(
              bottom: 20,
              right: 0,
              child: PieDePagina(),
            )
          ],
        ));
  }
}

//Clase que implementa la lista de paginas pintandolas y el archivo de rutas llamado pageRoutes
class _ListaOpciones extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Tema de la aplicacion
    final appTheme = Provider.of<ThemeChanger>(context).currentTheme;

    return ListView.separated(
      physics: BouncingScrollPhysics(),
      separatorBuilder: (context, i) => Divider(
        color: appTheme.primaryColorLight,
      ),

      //Aqui implemento el archivo de rutas llamado pagesRoutes
      itemCount: pageRoutes.length,
      itemBuilder: (context, i) => ListTile(
        leading: FaIcon(pageRoutes[i].icon, color: appTheme.accentColor),
        title: Text(pageRoutes[i].titulo),
        trailing: Icon(Icons.chevron_right, color: Colors.blue),
        onTap: () {
          //Implementamos el Navigator que recibe la ruta de MaterialPageRoute
          //para navegar al widget que selecciones
          Navigator.push(
              context,
              MaterialPageRoute(
                  //Funcion de flecha que regresa el pageRoute[i].page, con la pagina seleccionada
                  //ya que page es un widget declarado en la clase _Route
                  builder: (BuildContext context) => pageRoutes[i].page));
        },
      ),
    );
  }
}

//Widget que genera el icono menu de las tres rayas en el appBar en la parte izquierda
//y el pie de pagina  Dark Mode y Custom Theme
class _MenuPrincipal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.accentColor;

    return Drawer(
      child: Container(
        child: Column(
          children: <Widget>[
            //Aqui en en la parte superior de la pantalla se envuelve el Container()
            //en un SafeArea para que no pegue en la parte inicial de la pantalla
            SafeArea(
              child: Container(
                  padding: EdgeInsets.all(20),
                  width: double.infinity,
                  height: 210,
                  child: CircleAvatar(
                    backgroundColor: accentColor,
                    child: Text('Tabares Ingeniero',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                  )),
            ),

            //Este Expanded(), estira o expande la lista hasta el final de la pagina
            Expanded(child: _ListaOpciones()),

            //Este ListTitle() genera los elementos al final o pie de pagina  con sus colores
            ListTile(
              leading: Icon(Icons.lightbulb_outline, color: accentColor),
              title: Text('Dark Mode'),
              trailing: Switch.adaptive(
                  value: appTheme.darkTheme,
                  activeColor: accentColor,
                  onChanged: (value) => appTheme.darkTheme = value),
            ),

            //Este ListTitle() genera los elementos al final o pie de pagina  con sus colores
            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon(Icons.add_to_home_screen, color: accentColor),
                title: Text('Custom Theme'),
                trailing: Switch.adaptive(
                    value: appTheme.customTheme,
                    activeColor: accentColor,
                    onChanged: (value) => appTheme.customTheme = value),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class PieDePagina extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTheme = Provider.of<ThemeChanger>(context);
    final size = MediaQuery.of(context).size;

    return ButtonTheme(
      minWidth: size.width * 0.7,
      height: 40,
      child: RaisedButton(
          color: (appTheme.darkTheme)
              ? appTheme.currentTheme.accentColor
              : Color(0xff48A0EB),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(20))),
          child: Text('Tabares Ingeniero Desarrollador'),
          onPressed: () {}),
    );
  }
}
