import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:slideshow/src/theme/theme.dart';
import 'package:slideshow/src/widgets/slideshow.dart';

class SlideshowPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.purple,
      body: Column(
        children: <Widget>[
        Expanded(child: MySlideshow()),
        Expanded(child: MySlideshow())
        ],
      ),
    );
  }
}

class MySlideshow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
  
  final appTheme = Provider.of<ThemeChanger>(context);
  final accentColor = appTheme.currentTheme.accentColor;
  
    return Slideshow(
      bulletPrimario: 20,
      bulletSecundario: 12,

      //puntosArriba: false,
       colorPrimario: (appTheme.darkTheme ) ? accentColor : Color(0xffFF5A7E),
       //colorPrimario: Colors.green,
       colorSecundario: Colors.blue,
      //colorSecundario: Colors.purple,

      slides: <Widget>[
        SvgPicture.asset('assets/svgs/slide-1.svg'),
        SvgPicture.asset('assets/svgs/slide-2.svg'),
        SvgPicture.asset('assets/svgs/slide-3.svg'),
        SvgPicture.asset('assets/svgs/slide-4.svg'),
        SvgPicture.asset('assets/svgs/slide-5.svg'),
        Text('hello everybody'),
        Container(
          width: 200,
          height: 200,
          color: Colors.blue,
        )
      ],
    );
  }
}
