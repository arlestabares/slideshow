import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:slideshow/src/theme/theme.dart';
import 'package:slideshow/src/widgets/headers_.dart';

class HeadersPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final acccentColor = Provider.of<ThemeChanger>(context, listen: false)
        .currentTheme
        .accentColor;
    return Scaffold(body: HeaderWave(color: acccentColor));
  }
}
