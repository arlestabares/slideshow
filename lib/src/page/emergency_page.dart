import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:slideshow/src/widgets/boton_menu_principal.dart';
import 'package:slideshow/src/widgets/headers.dart';


class EmergencyPage extends StatelessWidget {
final items = <ItemBoton>[
  new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
  new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
  new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
  new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
  new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
  new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
  new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
  new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
  new ItemBoton( FontAwesomeIcons.carCrash, 'Motor Accident', Color(0xff6989F5), Color(0xff906EF5) ),
  new ItemBoton( FontAwesomeIcons.plus, 'Medical Emergency', Color(0xff66A9F2), Color(0xff536CF6) ),
  new ItemBoton( FontAwesomeIcons.theaterMasks, 'Theft / Harrasement', Color(0xffF2D572), Color(0xffE06AA3) ),
  new ItemBoton( FontAwesomeIcons.biking, 'Awards', Color(0xff317183), Color(0xff46997D) ),
];

  @override
  Widget build(BuildContext context) {
    List<Widget> itemMap = items
    //.map(( item) => BotonMenuPrincipal(
        .map((item) => FadeInLeft(
              duration: Duration(milliseconds:200),
              child: BotonMenuPrincipal(
                icon: item.icon,
                texto: item.texto,
                color1: item.color1,
                color2: item.color2,
                onPress: () {
                  print('imprmir en consola');
                },
              ),
        ))
        .toList();

    return Scaffold(
      //backgroundColor: Colors.red,
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 270),
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                // SizedBox(height: 40, ),
                ...itemMap,
                _BotonMenuPrincipalConfiguracion(),
              ],
            ),
          ),
          _EncabezadoIconHeader()
        ],
      ),
    );
  }
}

class _EncabezadoIconHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        IconHeader(
            icon: FontAwesomeIcons.plus,
            titulo: 'Asistencia Medica',
            subtitulo: 'Haz solicitado',
            color1: Color(0xff536CF6),
            color2: Color(0xff66A9F2)),
        //posicion y configuracion del boton de tres puntos en la parte derecha del encabezdo principal
        Positioned(
            right: 10,
            top: 50,
            child: RawMaterialButton(
                onPressed: () {},
                shape: CircleBorder(),
                padding: EdgeInsets.all(25.0),
                child: FaIcon(
                  FontAwesomeIcons.ellipsisV,
                  color: Colors.white,
                )))
      ],
    );
  }
}

class ItemBoton {
  ItemBoton(this.icon, this.texto, this.color1, this.color2);

  final Color color1;
  final Color color2;
  final IconData icon;
  final String texto;
}
//Clase que contiene el BotonMenuPrincipal encargado de toda la configuracion del mismo
class _BotonMenuPrincipalConfiguracion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BotonMenuPrincipal(
      icon: FontAwesomeIcons.carCrash,
      texto: 'Motor Accidente',
      color1: Color(0xff906EF5),
      color2: Color(0xff6989F5),
      onPress: () {
        print('presionado');
      },
    );
  }
}
//Widget encargado de la configuracion del encabezado de la interfaz principal
class PageHeader extends StatelessWidget {
  const PageHeader();
  @override
  Widget build(BuildContext context) {
    return IconHeader(
      icon: FontAwesomeIcons.plusCircle,
      subtitulo: 'Has Solicitado',
      titulo: 'Asistencia Medica',
      color1: Color(0xff526BF6),
      color2: Color(0xff67ACF2),
    );
  }
}
