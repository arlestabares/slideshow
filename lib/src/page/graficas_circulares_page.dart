import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:slideshow/src/theme/theme.dart';
import 'package:slideshow/src/widgets/radial_progress.dart';

class GraficasCircularesPage extends StatefulWidget {
  @override
  _GraficasCircularesPageState createState() => _GraficasCircularesPageState();
}

class _GraficasCircularesPageState extends State<GraficasCircularesPage> {
  double porcentaje = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.refresh),
            onPressed: () {
              setState(() {
                porcentaje += 10;
                if (porcentaje > 100) {
                  porcentaje = 0;
                }
              });
            }),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                CustomRadialProgress(porcentaje: porcentaje , color: Colors.blue,
                ),
                CustomRadialProgress(porcentaje: porcentaje * 1.2,  color: Colors.red,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                CustomRadialProgress( porcentaje: porcentaje * 1.5, color: Colors.blue,
                ),
                CustomRadialProgress( porcentaje: porcentaje * 1.8,color: Colors.red,
                ),
              ],
            )
          ],
        ));
  }
}

class CustomRadialProgress extends StatelessWidget {
  final double porcentaje;
  final Color color;
  const CustomRadialProgress({@required this.porcentaje, @required this.color});

  @override
  Widget build(BuildContext context) {
  
  final appTheme = Provider.of<ThemeChanger>(context).currentTheme;
    return Container(
      width: 200,
      height: 200,
      // color: Colors.blue,
      child: RadialProgress(
        porcentaje: porcentaje,
       // colorPrimario: Colors.green,
       colorPrimario: this.color,
        colorSecundario: appTheme.textTheme.bodyText1.color,
        grosorPrimario: 4.0,
        grosorSecundario: 10.0,
      ),
    );
  }
}
