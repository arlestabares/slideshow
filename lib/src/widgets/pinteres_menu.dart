import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PinterestButton {
  final Function onPreseed;
  final IconData icon;

  PinterestButton(
      {@required this.onPreseed,
      @required this.icon}); //Entre llaves..{}, asi la persona puede mandar esto como quiera.

}

class PinterestMenu extends StatelessWidget {
  final bool mostrar;

  final Color backgroundColor;
  final Color activeColor; //
  final Color inactiveColor;
  
  final List<PinterestButton> items;

  PinterestMenu(
      {this.mostrar = true,
      this.backgroundColor = Colors.white,
      this.activeColor = Colors.black,
      this.inactiveColor = Colors.blueGrey,
      @required this.items
      
      });

  // final List<PinterestButton> items = [
  //   PinterestButton(icon: Icons.pie_chart,onPreseed: () { print('Icon pie_chat'); }),
  //   PinterestButton( icon: Icons.search, onPreseed: () {  print('Icon search'); }),
  //   PinterestButton(icon: Icons.notifications, onPreseed: () {  print('Icon notifications');}),
  //   PinterestButton( icon: Icons.supervised_user_circle,onPreseed: () { print('Icon supervised_user_circle');})
  // ];


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      //Aqui dentro de este Widget tengo una instancia de mi modelo _MenuModel()
      create: (_) => new _MenuModel(),
      //Toda la siguiente configuracion,hace referencia al contenedor
      //que tendra los items del _MenuItems().
      child: AnimatedOpacity(
          duration: Duration(milliseconds: 250),
          opacity: (mostrar) ? 1 : 0,
          child: Builder(
            builder: (BuildContext context) {
             Provider.of<_MenuModel>(context, listen: false).backgroundColor = this.backgroundColor;
             Provider.of<_MenuModel>(context, listen: false).activeColor = this.activeColor;
             Provider.of<_MenuModel>(context, listen: false).inactiveColor = this.inactiveColor;
            
              return _PinterestMenuBackground(
                child: _MenuItems(items),
              );
            },
          )),
    );
  }
}

//Configuracion del contenedor de items para construir los bordes y los colores
class _PinterestMenuBackground extends StatelessWidget {
  final Widget child;
  const _PinterestMenuBackground({this.child});

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Provider.of<_MenuModel>(context).backgroundColor;

    return Container(
      child: child,
      width: 250,
      height: 60,
      decoration: BoxDecoration(
          //color: Colors.white,
          color: backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(100)),
          boxShadow: <BoxShadow>[
            BoxShadow(color: Colors.black38, blurRadius: 10, spreadRadius: -5)
          ]),
    );
  }
}

//_MenuItems genera una lista de items  dentro del contenedor.En pocas palabras
//es el que crea los items.
class _MenuItems extends StatelessWidget {
  final List<PinterestButton> menuItems;
  _MenuItems(this.menuItems);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(
            menuItems.length, (i) => _PinterestMenuButton(i, menuItems[i])));
  }
}

//Personalizacion de los items del menu de seleccion, en pocas palabras se encarga
//de cada uno de los botones y sus gestos asociados. (GestureDetector)
class _PinterestMenuButton extends StatelessWidget {
  final int index;
  final PinterestButton item;

  _PinterestMenuButton(this.index, this.item);

  @override
  Widget build(BuildContext context) {
    //Hacemos la extraccion del item selecionado, mediante la siguiente linea de codigo.
    final itemSeleccionado = Provider.of<_MenuModel>(context).itemSeleccionado;
    
    //Obtengo una instancia con menuModel para utilizar sus variables definidas
    final menuModel = Provider.of<_MenuModel>(context);
    
    return GestureDetector(
      //onTap: item.onPreseed,
      onTap: () {
        Provider.of<_MenuModel>(context, listen: false).itemSeleccionado = index;
        item.onPreseed();
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        child: Icon(
          item.icon,
          size: (itemSeleccionado == index) ? 30 : 25,
          color: (itemSeleccionado == index) ? menuModel.activeColor : menuModel.inactiveColor,
        ),
      ),
    );
  }
}

//Clase encargada de notificar a los Widgets que estén escuchando
//o que estén vinculadas al ChangeNotifierProvider,dicho Widget se débe utilizar
// en la parte mas alta del arbol de Widgets de mi App
class _MenuModel with ChangeNotifier {
  int _itemSeleccionado = 0;

  Color backgroundColor = Colors.white;
  Color activeColor     = Colors.black;
  Color inactiveColor   = Colors.blueGrey;

  int get itemSeleccionado => this._itemSeleccionado;

  set itemSeleccionado(int index) {
    this._itemSeleccionado = index;
    notifyListeners();
  }
}
