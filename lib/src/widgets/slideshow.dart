import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

class Slideshow extends StatelessWidget {
  final List<Widget> slides;
  final bool puntosArriba;
  final Color colorPrimario;
  final Color colorSecundario;
  final double bulletPrimario;
  final double bulletSecundario;

  Slideshow(
      {@required this.slides,
      this.puntosArriba = false,
      this.colorPrimario = Colors.red,
      this.colorSecundario = Colors.orange,
      this.bulletPrimario = 12.0,
      this.bulletSecundario = 12.0});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => _SlideshowModel(),
        child: SafeArea(
          child: Center(
          child: Builder(
            //El Widget builder espera que todo lo anterior se construya, y cuando
            //tiene el contexto anterior creado dispara el builder que retornara el widget
            //_CrearEstructuraSlideshow()
            builder: (BuildContext context) {
              //Aqui ponemos el Provider justo en el nivel donde lo inicializamos
              Provider.of<_SlideshowModel>(context).colorPrimario =
                  this.colorPrimario;
              Provider.of<_SlideshowModel>(context).colorSecundario =
                  this.colorSecundario;

              Provider.of<_SlideshowModel>(context).bulletPrimario =
                  this.bulletPrimario;
              Provider.of<_SlideshowModel>(context).bulletSecundario =
                  this.bulletSecundario;

              return _CrearEstructuraSlideshow(
                  puntosArriba: puntosArriba,
                  slides: slides,
                  colorPrimario: colorPrimario,
                  colorSecundario: colorSecundario);
            },
          )
         ),
        )
    );
  }
}

class _CrearEstructuraSlideshow extends StatelessWidget {
  const _CrearEstructuraSlideshow({
    @required this.puntosArriba,
    @required this.slides,
    @required this.colorPrimario,
    @required this.colorSecundario,
  });

  final bool puntosArriba;
  final List<Widget> slides;
  final Color colorPrimario;
  final Color colorSecundario;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        if (this.puntosArriba)
          _Dots(this.slides.length, this.colorPrimario,
              this.colorSecundario), //saber el numero de puntos
        Expanded(child: _Slides(this.slides)),
        if (!this.puntosArriba)
          _Dots(this.slides.length, this.colorPrimario, this.colorSecundario),
      ],
    );
  }
}

class _Dots extends StatelessWidget {
  final int totalSlides;
  final Color colorPrimario;
  final Color colorSecundario;

  _Dots(this.totalSlides, this.colorPrimario, this.colorSecundario);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      //color: Colors.red,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        // children: <Widget>[
        // _Dot(0),
        // _Dot(1),
        // _Dot(2)],
        //recibo el total de slides, y recibo la posicion (i),
        //funcion de flecha y retorno un _Dot(i)
        children: List.generate(this.totalSlides, (i) => _Dot(i)),
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  //_Dot()...Representa un punto dentro de la clase _Dots()
  final int index;

  _Dot(this.index);

  @override
  Widget build(BuildContext context) {
    //final pageViewIndex = Provider.of<_SlideshowModel>(context).currentPage;//el SliderModel contiene lo necesario para enviar al BoxDecoration
    final slideshowModel = Provider.of<_SlideshowModel>(context);
    // final tamano = (slideshowModel.currentPage == index) ? slideshowModel.bulletPrimario
    //     :slideshowModel.bulletSecundario;
    double tamano;
    Color color;

    if (slideshowModel.currentPage >= index - 0.5 &&
        slideshowModel.currentPage < index + 0.5) {
      tamano = slideshowModel.bulletPrimario;
      color = slideshowModel.colorPrimario;
    } else {
      tamano = slideshowModel.bulletSecundario;
      color = slideshowModel.colorSecundario;
    }

    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      //Tarea.. si el bullet es el que esta activo, este bullet tiene que ser
      //del tamaño del bulletPrimario, sino sera del tamaño  bulletSecundario.
      width: tamano,
      height: tamano,
      //width: 12,
      //height: 12,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
          color: color,
          // color: (slideshowModel.currentPage >= index - 0.5 &&
          //         slideshowModel.currentPage < index + 0.5)
          //     ? slideshowModel.colorPrimario
          //     : slideshowModel.colorSecundario,
          shape: BoxShape.circle),
    );
  }
}

//SvgPicture.asset('assets/svgs/slide-1.svg'),
class _Slides extends StatefulWidget {
  final List<Widget> slides;

  _Slides(this.slides);

  @override
  __SlidesState createState() => __SlidesState();
}

class __SlidesState extends State<_Slides> {
  final pageViewController = new PageController();

  @override
  void initState() {
    super.initState();

    pageViewController.addListener(() {
      //print('Pagina actual ${pageViewController.page}');
      //justo aqui en el addListener() actualizo el provider, sliderModel
      Provider.of<_SlideshowModel>(context, listen: false).currentPage =
          pageViewController.page;
    });
  }

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: PageView(
        controller: pageViewController,
        //el PageView es una coleccion de imagenes
        // children: <Widget>[
        //   _Slide('assets/svgs/slide-1.svg'),
        //   _Slide('assets/svgs/slide-2.svg'),
        //   _Slide('assets/svgs/slide-3.svg'),
        // ],
        children: widget.slides.map((slide) => _Slide(slide)).toList(),
      ),
    );
  }
}

class _Slide extends StatelessWidget {
  final Widget slide;
  const _Slide(this.slide);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.all(30),
        child: slide);
    //SvgPicture.asset('assets/svgs/slide-1.svg');
  }
}

class _SlideshowModel with ChangeNotifier {
  //Forma y color de cada uno de los PUNTOS
  double _currentPage = 0;
  Color _colorPrimario = Colors.blue;
  Color _colorSecundario = Colors.white;
  double _bulletPrimario = 12;
  double _bulletSecundario = 12;

  double get currentPage => this._currentPage;

  set currentPage(double currentPage) {
    this._currentPage = currentPage;

    print(currentPage);
    notifyListeners();
  }

  Color get colorPrimario => this._colorPrimario;
  set colorPrimario(Color color) {
    this._colorPrimario = color;
  }

  Color get colorSecundario => this._colorSecundario;
  set colorSecundario(Color color) {
    this._colorSecundario = color;
  }

  double get bulletPrimario => this._bulletPrimario;
  set bulletPrimario(double tamano) {
    this._bulletPrimario = tamano;
  }

  double get bulletSecundario => this._bulletSecundario;
  set bulletSecundario(double tamano) {
    this._bulletSecundario = tamano;
  }
}
