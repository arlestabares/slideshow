import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

//Clase encargada de la configuracion de un BotonMenuPrincipal, de los items internos
//y del posicionamineto y color de los mismos
class BotonMenuPrincipal extends StatelessWidget {
  @required
  final IconData icon;
  @required
  final String texto;
  final Color color1;
  final Color color2;
  @required
  final Function onPress;

  const BotonMenuPrincipal(
      {this.icon = FontAwesomeIcons.circle,
      this.texto,
      this.color1 = Colors.grey,
      this.color2 = Colors.blueGrey,
      this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onPress,
      child: Stack(
        children: <Widget>[
          _BotonMenuPrincipalBackground(
            this.icon,
            this.color1,
            this.color2,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 140, width: 40),
              FaIcon(this.icon, color: Colors.white, size: 40),
              SizedBox(width: 20),
              Expanded(
                  child: Text(this.texto,
                      style: TextStyle(color: Colors.white, fontSize: 18))),
              FaIcon(FontAwesomeIcons.chevronRight, color: Colors.white),
              SizedBox(width: 40)
            ],
          )
        ],
      ),
    );
  }
}

//Configuracion del BotonMenuPrincipal, gradiente, icono y opacidad.
class _BotonMenuPrincipalBackground extends StatelessWidget {
  final IconData icon;
  final Color color1;
  final Color color2;

  const _BotonMenuPrincipalBackground(this.icon, this.color1, this.color2);

  @override
  Widget build(BuildContext context) {
  
  //Contenedor del BotonMenuPrincipal, un icono, gradiente y opacidad.
    return Container(
      child: ClipRRect(
        //ClipRRectnos permite cortar los bordes por si algo se sale del espacio definido en un container cualquiera
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: <Widget>[
            Positioned(
              right: -20,
              top: -20,
              child: FaIcon(
                this.icon,
                size: 150,
                color: Colors.white.withOpacity(0.2),
              ),
            )
          ],
        ),
      ),
      //Configuracion del contenedor BotonMenuPrincipal,ancho,largo, gradiente y opacidad.
      width: double.infinity,
      height: 100,
      margin: EdgeInsets.all(20),
      decoration: BoxDecoration(
          // color: Colors.red,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black.withOpacity(0.2),
                offset: Offset(4, 6),
                blurRadius: 10),
          ],
          borderRadius: BorderRadius.circular(15),
          gradient: LinearGradient(colors: <Color>[
            this.color1,
            this.color2,
          ])),
    );
  }
}
