import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:slideshow/src/page/emergency_page.dart';
import 'package:slideshow/src/page/graficas_circulares_page.dart';
import 'package:slideshow/src/page/headers_page.dart';
import 'package:slideshow/src/page/pinteres_page.dart';
import 'package:slideshow/src/page/slideshow_page.dart';
import 'package:slideshow/src/page/slider_list_page.dart';
import 'package:slideshow/src/retos/cuadrado_animado.dart';

final pageRoutes = <_Route>[
  _Route(FontAwesomeIcons.slideshare, 'slideshow', SlideshowPage()),
  _Route(FontAwesomeIcons.ambulance, 'Emergencia', EmergencyPage()),
  _Route(FontAwesomeIcons.heading, 'Encabezados', HeadersPage()),
  _Route(FontAwesomeIcons.peopleCarry, 'Cuadrado Animado', CuadradoAnimadoPage()),
  _Route(FontAwesomeIcons.circleNotch, 'Barra de Progreso',GraficasCircularesPage()),
  _Route(FontAwesomeIcons.pinterest, 'Pinteres', PinteresPage()),
  _Route(FontAwesomeIcons.mobile, 'Slivers', SliverListPage()),
];

class _Route {
  final IconData icon;
  final String titulo;
  final Widget page;

  _Route(this.icon, this.titulo, this.page);
}
