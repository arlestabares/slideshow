import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

class CircularProgreesPage extends StatefulWidget {
  @override
  _CircularProgreesPageState createState() => _CircularProgreesPageState();
}

class _CircularProgreesPageState extends State<CircularProgreesPage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  double porcentaje = 0.0;
  double nuevoPorcentaje = 0.0;

  @override
  void initState() {
    controller = new AnimationController(
        vsync: this, duration: Duration(milliseconds: 800));
    controller.addListener(() {
      print('valor controller: ${controller.value}');
      //el tercer parametro de lerpDouble es una medida de tiempo.
      setState(() {
        porcentaje = lerpDouble(porcentaje, nuevoPorcentaje,
            controller.value); //Interpolar linealmente entre dos números
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.refresh),
          backgroundColor: Colors.pink,
          onPressed: () {
            controller.forward(from: 0.0);

            setState(() {
              porcentaje = nuevoPorcentaje;
              nuevoPorcentaje += 10;
              if (nuevoPorcentaje > 100) {
                nuevoPorcentaje = 0;
                porcentaje = 0;
              }
            });
          }),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(4),
          width: 300,
          height: 300,
          //color: Colors.red,
          child: CustomPaint(
            //Al CustomPaint le llega un painter de tipo CustomPaint
            painter: _MiRadialProgress(porcentaje),
          ),
        ),
      ),
    );
  }
}

class _MiRadialProgress extends CustomPainter {
  final porcentaje;
  _MiRadialProgress(this.porcentaje);

  @override
  void paint(Canvas canvas, Size size) {
    //Circulo Completado
    final paint = new Paint() //Creamos el Lapiz y definimos unas carateristicas
      ..strokeWidth = 4.0 //ancho del lapiz
      ..color = Colors.grey //color del lapiz
      ..style = PaintingStyle.stroke; //estilo del lapiz

    Offset center = new Offset(size.width * 0.5, size.height / 2);
    double radio = min(size.width * 0.5, size.height * 0.5);

    //Ahora le digo a  Flutter cómo quiero que se dibuje el circulo y el centro
    canvas.drawCircle(center, radio, paint);

    //Creacion del ARCO O curva

    final paintArco = new Paint()
      ..strokeWidth = 10
      ..color = Colors.pink
      ..style = PaintingStyle.stroke;

    //Parte que se debera ir llenando del arco definido
    double arcAngle = 2 * pi * (porcentaje / 100);
    //dibujamos el arco u angulo con drawArc()
    canvas.drawArc(
        //Rect es como el espacio donde deseamos poner o ubicar el angulo
        Rect.fromCircle(center: center, radius: radio),
        -pi / 2,
        arcAngle,
        false,
        paintArco);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
